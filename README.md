# MongoDB like filters for javascript

This library implements part of the MongoDB syntax in plain javascript.

For details about the [MongoDB syntax](https://docs.mongodb.com/manual/reference/operator/query/).

# Build

To build this project as a front enabled bundle:
```bash
# Run webpack and bundle things in /dist
npm run build
```

# Usage

To use this library as a dependency:
```bash
# Install it
npm install "git+https://gitlab.cern.ch/mro/common/www/mfilter.js.git"
```

Then components can be imported:
```js
// using ES6 imports
import { mfilter } from '@cern/mfilter';

// using Node.js require
const { mfilter } = require('@cern/mfilter');
```
