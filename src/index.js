// @ts-check
/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-05-04T09:21:41+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

import { delay } from "@cern/nodash";

/**
 * for vite-plugin-dts to expose types
 * @typedef {import("./types.d.ts")} mfilter
 */

// release processing loop every X ms
const ReschedInterval = 10;

/**
 * @brief return true for object like parameters
 * @details this isPlainObject implementation is really dedicated to mfilter
 * trees, rejecting RegExp and Arrays objects
 * @param {Object} o
 * @returns {boolean}
 */
function isPlainObject(o) {
  return (typeof o === "object") &&
    !(o instanceof RegExp) &&
    !(o instanceof Array);
}

/**
 * @brief basic noop implementation
 */
function noop() {}

/**
 * @param {mfilter.BaseValue} value
 * @param {Array<mfilter.BaseValue>} exp
 * @return {boolean}
 */
function inOpSimple(value, exp) {
  return (exp ?? []).some(function(v) {
    return (v instanceof RegExp) ? v.test(value.toString()) : (value === v);
  });
}

const compOp = {
  /** @type {(value: mfilter.BaseValue, exp: mfilter.BaseValue) => boolean} */
  $eq: function(value, exp) { return value === exp; },
  /** @type {(value: number, exp: number) => boolean} */
  $lt: function(value, exp) { return value < exp; },
  /** @type {(value: number, exp: number) => boolean} */
  $lte: function(value, exp) { return value <= exp; },
  /** @type {(value: number, exp: number) => boolean} */
  $gt: function(value, exp) { return value > exp; },
  /** @type {(value: number, exp: number) => boolean} */
  $gte: function(value, exp) { return value >= exp; },
  /** @type {(value: mfilter.BaseValue, exp: mfilter.BaseValue[]) => boolean} */
  $in: function(value, exp) {
    if (value instanceof Array) {
      // $FlowIgnore: special case where value itself is an array
      return value.some((velt) => inOpSimple(velt, exp));
    }
    return inOpSimple(value, exp);
  },
  /** @type {(value: mfilter.BaseValue, exp: mfilter.BaseValue[]) => boolean} */
  $nin: function(value, exp) { return !compOp.$in(value, exp); },
  /** @type {(value: mfilter.BaseValue, exp: mfilter.BaseValue) => boolean} */
  $ne: function(value, exp) { return value !== exp; },
  /** @type {(value: mfilter.BaseValue, exp: mfilter.BaseValue) => boolean} */
  $not: function(value, exp) { return value !== exp; },
  /** @type {(value: mfilter.BaseValue, exp: mfilter.BaseValue) => boolean} */
  $exists: function(value, exp) {
    return (exp ? (value !== undefined) : (value === undefined));
  },
  /** @type {(value: string, exp: RegExp) => boolean} */
  $regex: function(value, exp) {
    return exp.test(value);
  },
  $options: function() { return true; }
};

const arrayOp = {
  /** @type {(query: mfilter.Query[], item: mfilter.Item) => boolean} */
  $and: function(query, item) { return _validateArray(query, item); },
  /** @type {(query: mfilter.Query[], item: mfilter.Item) => boolean} */
  $or: function(query, item) { return _validateArray(query, item, true); }
};

/**
 * @brief validate an item against an array of queries
 * @param {mfilter.Query[]} query
 * @param {mfilter.Item} item
 * @param {?boolean=} any
 * @return {boolean}
 */
function _validateArray(query, item, any) {
  return query[any ? "some" : "every"](function(value) {
    return _validate(item, value, null, false);
  });
}

/**
 * @brief validate an item against a query
 * @param {mfilter.Item} item
 * @param {mfilter.Query} query
 * @param {?string=} parentKey
 * @param {?boolean=} any
 * @return {boolean}
 */
function _validate(item, query, parentKey, any) {
  return Object.entries(query)[any ? "some" : "every"](function([ key, value ]) {
    const op = arrayOp?.[/** @type {keyof arrayOp} */ (key)];
    if (op) { return op(/** @type {mfilter.Query[]} */ (value), item); }
    else if (isPlainObject(value)) {
      return (key === "$not") ?
        !_validate(item, /** @type {mfilter.Query} */ (value), parentKey, false) :
        _validate(item, /** @type {mfilter.Query} */ (value), key, false);
    }
    else {
      const cop = /** @type {(value: any, exp: any) => boolean} */ (compOp?.[/** @type {keyof compOp} */ (key)] ?? compOp["$eq"]);
      return cop(item[parentKey ?? key], value);
    }
  });
}

/**
 * @param {mfilter.Query|mfilter.Op} query
 */
export function mprepare(query) {
  // eslint-disable-next-line complexity
  Object.entries(query).forEach(function([ key, value ]) {
    if (arrayOp[/** @type {keyof arrayOp} */ (key)]) {
      value.forEach(mprepare);
    }
    else if (isPlainObject(value)) {
      mprepare(value);
    }
    else if (key === "$regex") {
      if (typeof value === "string" || "$options" in query) {
        query[key] = new RegExp(value, query["$options"]);
      }
    }
    else if (value instanceof RegExp) {
      if (key === "$eq") {
        query["$regex"] = value;
        delete query["$eq"];
      }
      else {
        query[key] = { $regex: value };
      }
    }
  });
}

/**
 * @param {mfilter.Item[]} items
 * @param {mfilter.Query} query
 * @param {mfilter.Context} ctx
 * @return {Promise<mfilter.Item[]>}
 */
async function _filterCtrl(items, query, ctx) {
  if (!ctx.pending) {
    throw new Error("aborted");
  }

  for (; ctx.idx < items.length; ++ctx.idx) {
    const item = items[ctx.idx];
    if (_validate(item, query, null, false)) {
      ctx.result.push(item);
      ctx.cb(item, ctx.idx, true);
    }
    else {
      ctx.cb(item, ctx.idx, false);
    }

    if (!ctx.pending) {
      throw new Error("aborted");
    }
    const nowStamp = Date.now();
    if (nowStamp >= (ctx.stamp + ReschedInterval)) {
      ctx.stamp = nowStamp;
      ctx.idx++;
      // force a reschedule
      await delay(0);
    }
  }
  return ctx.result;
}

/**
 * @brief filter an array according to mongoDB like query
 * @param {mfilter.Item[]} items array to filter
 * @param {mfilter.Query} query the query to run
 * @return {mfilter.Item[]} filtered array
 */
export function mfilter(items, query) {
  mprepare(query);
  return items.filter((i) => _validate(i, query, null, false));
}

/**
 * @brief filter an array of item with progress and cancellation support
 * @param {mfilter.Item[]} items array to filter
 * @param {mfilter.Query} query the query to run
 * @param  {?mfilter.Progress=} cb
 * @return {mfilter.Control} a ctrl object
 */
export function mfilterCtrl(items, query, cb) {
  mprepare(query);
  var ctx = {
    pending: true,
    cb: cb ?? noop,
    stamp: Date.now(),
    idx: 0,
    result: []
  };
  var ctrl = {
    abort: function() { ctx.pending = false; },
    promise: Promise.resolve().then(() => _filterCtrl(items, query, ctx))
  };
  return ctrl;
}

/**
 * @brief validate an element according to mongoDB like query
 * @param {mfilter.Item} item item to validate
 * @param {mfilter.Query} query item to validate
 * @return boolean
 */
export function mvalidate(item, query) {
  mprepare(query);
  return _validate(item, query, null, false);
}

