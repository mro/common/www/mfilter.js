import { expectType } from "tsd";

import { mfilter, mfilterCtrl, mvalidate } from ".";

(async function(): Promise<void> {
  const sampleData = [ { test: 42 }, { another: "test" } ];

  expectType<mfilter.Item[]>(
    mfilter(sampleData, { test: { $eq: 42 }, another: "test" }));

  expectType<boolean>(
    mvalidate(sampleData, { test: { $eq: 42 }, another: "test" }));

  var ctrl: mfilter.Control = mfilterCtrl(sampleData, { $not: { test: 42 } });

  ctrl = mfilterCtrl(sampleData, { $not: { test: 42 } },
    (item: mfilter.Item, idx: number, isValid: boolean) => {
      console.log(item, idx, isValid);
    });

  expectType<mfilter.Item[]>(await ctrl.promise);
  ctrl.abort();
}());
