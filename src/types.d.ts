// / <reference types="node" />

export = mfilter;
// export module "mfilter";
export as namespace mfilter;

export namespace mfilter {
  type BaseValue = string|number|RegExp|boolean

  interface Op {
    $eq?: BaseValue;
    $lt?: number;
    $lte?: number;
    $gt?: number;
    $gte?: number;
    $in?: Array<BaseValue>;
    $nin?: Array<BaseValue>;
    $ne?: BaseValue;
    $exists?: boolean;
    $regex?: RegExp|string;
    $options?: any;
    $not?: Op|Query|BaseValue;
    $or?: Array<Query>;
    $and?: Array<Query>;
    [key: string]: any;
  }

  type Item = { [key: string]: any }

  interface Query {
    // had to use any to overlap or/and/not declarations
    [key: string]: string|number|RegExp|Op|any;
    $or?: Array<Query>;
    $and?: Array<Query>;
    $not?: Query;
  }

  type Progress = (item: Item, idx: number, isValid: boolean) => any

  interface Control {
    abort(): void;
    promise: Promise<Array<Item>>;
  }

  interface Context {
    pending: boolean;
    idx: number;
    cb: Progress;
    stamp: number;
    result: Array<Item>;
  }
}
