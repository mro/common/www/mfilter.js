// @ts-check

import { expect } from "chai";
import { mfilter, mfilterCtrl } from "../src/index.js";
import { set } from "@cern/nodash";

/*::
declare var serverRequire: (string) => any
*/


describe("MFilter", function() {
  [
    { filter: { user: "bob" },
      result: [ "bob" ] },
    { filter: { user: { $eq: "bob" } },
      result: [ "bob" ] },
    { filter: { $or: [ { $and: [ { user: "bob" } ] } ] },
      result: [ "bob" ] },
    /* objects */
    { filter: { age: { $gte: 10, $lte: 44 } },
      result: [ "bob", "raoul" ] },
    /* $or */
    { filter: { $or: [ { user: "bob" }, { age: 50 } ] },
      result: [ "bob", "carol" ] },
    /* $and */
    { filter: { $and: [ { age: { $gte: 10 } }, { age: { $lte: 21 } } ] },
      result: [ "bob" ] },
    /* $not */
    { filter: { $not: { user: "bob" } },
      result: [ "carol", "raoul" ] },
    { filter: { user: { $not: "bob" } },
      result: [ "carol", "raoul" ] },
    { filter: { user: { $not: { user: "bob" } } },
      result: [ "carol", "raoul" ] },

    /* $in */
    { filter: { user: { $in: [ "carol", "raoul" ] } },
      result: [ "carol", "raoul" ] },
    { filter: set({}, [ "age", "$not", "$in" ], [ 21, 50 ]),
      result: [ "raoul" ] },
    /* $nin */
    { filter: { user: { $nin: [ "carol", "raoul" ] } },
      result: [ "bob" ] },
    { filter: set({}, [ "age", "$not", "$nin" ], [ 21, 50 ]),
      result: [ "bob", "carol" ] },
    /* no such prop */
    { filter: { admin: { $eq: true, $in: [ true ], $exists: true } },
      result: [ "raoul" ] },
    /* exists */
    { filter: { admin: { $exists: true } }, result: [ "raoul" ] },
    { filter: { admin: { $exists: false } }, result: [ "bob", "carol" ] },
    /* regex */
    { filter: { user: { $regex: /a[ro]/ } }, result: [ "carol", "raoul" ] },
    { filter: { user: /a[ro]/ }, result: [ "carol", "raoul" ] },
    { filter: { user: { $regex: "a[ro]" } }, result: [ "carol", "raoul" ] }

  ].forEach((t) => {
    // eslint-disable-next-line max-len
    it(`filters with ${JSON.stringify(t.filter)} -> ${JSON.stringify(t.result)}`, () => {
      const ret = mfilter([
        { user: "bob", age: 21 },
        { user: "carol", age: 50 },
        { user: "raoul", age: 44, admin: true }
      // @ts-ignore
      ], t.filter);
      expect(ret.map((v) => v.user)).to.deep.equal(t.result);
    });
  });

  [
    { filter: { admin: { $gre: true, $lte: true } }, result: [ "raoul" ] }
  ].forEach((t) => {
    // eslint-disable-next-line max-len
    it(`handles special case ${JSON.stringify(t.filter)} -> ${JSON.stringify(t.result)}`, () => {
      const ret = mfilter([
        { user: "bob", age: 21 },
        { user: "carol", age: 50 },
        { user: "raoul", age: 44, admin: true }
        // $FlowIgnore
      ], t.filter);
      expect(ret.map((v) => v.user)).to.deep.equal(t.result);
    });
  });

  describe("Promise", function() {
    it("can run asynchronuously", () => {
      var count = 0;
      return mfilterCtrl([
        { user: "bob", age: 21 },
        { user: "carol", age: 50 },
        { user: "raoul", age: 44, admin: true }
      ], set({}, [ "age", "$lte" ], 44), () => (++count)).promise
      .then(
        (ret) => expect(ret.map((v) => v.user)).to.deep.equal([ "bob", "raoul" ]))
      .then(() => expect(count).to.equal(3));
    });

    it("can stop from progress callback", () => {
      var count = 0;
      var ctrl = mfilterCtrl([
        { user: "bob", age: 21 },
        { user: "carol", age: 50 },
        { user: "raoul", age: 44, admin: true }
      ], set({}, [ "age", "$lte" ], 44), () => { ++count; ctrl.abort(); });
      return ctrl.promise
      .then(() => { throw "should fail"; }, () => expect(count).to.equal(1));
    });

    it("can stop from another context", () => {
      // lots of bobs
      const user = { user: "bob", age: 21 };
      var data = Array.from({ length: 1000000 }, () => user);
      var count = 0;
      var ctrl = mfilterCtrl(data, { user: /[b][o][b]/ }, () => {
        if (count++ === data.length / 2) {
          /* delaying on purpose */
          Promise.resolve().then(() => ctrl.abort());
        }
      });
      return ctrl.promise
      .then(
        () => { throw "should fail"; },
        () => expect(count).to.be.lessThan(data.length));
    });
  });

});
